<?php
/**
 * This file adds the Home Page to the El Dorado Tours Theme.
 *
 * @author Mauricio Alvarez
 * @subpackage Customizations
 */
 
//* Enqueue scripts
add_action( 'wp_enqueue_scripts', 'eldorado_front_page_enqueue_scripts' );
function eldorado_front_page_enqueue_scripts() {
	
	//* Load scripts only if custom background is being used
	if ( ! get_option( 'eldorado-home-image' ) )
		return;

	//* Enqueue Backstretch scripts & Fadeup Effect
	wp_enqueue_script( 'eldorado-backstretch-fadeup', get_bloginfo( 'stylesheet_directory' ) . '/js/min/home-min.js', array( 'jquery' ), '1.0.0' ); //home.js contain backstretch.js & fadeup.js
	wp_enqueue_script( 'eldorado-backstretch-set', get_bloginfo( 'stylesheet_directory' ).'/js/partials/backstretch-set.js' , array( 'jquery', 'eldorado-backstretch-fadeup' ), '1.0.0' );

	wp_localize_script( 'eldorado-backstretch-set', 'BackStretchImg', array( 'src' => str_replace( 'http:', '', get_option( 'eldorado-home-image' ) ) ) );
	
}

 
add_action( 'genesis_meta', 'designnify_home_page_genesis_meta' );
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */
function designnify_home_page_genesis_meta() {
	
	if ( is_active_sidebar( 'home-top' ) || is_active_sidebar( 'home-middle' ) ) { 
		
		//* Add front-page body class
		add_filter( 'body_class', 'eldorado_body_class' );
		function eldorado_body_class( $classes ) {

   			$classes[] = 'eldorado-front-page';
   			return $classes;

		}
		
		//* Force full width content layout
		add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );
				
		//* Add home body class
		add_filter( 'body_class', 'eldorado_body_class' );
		
		//* Remove breadcrumbs
		remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

		//* Remove the default Genesis loop
		remove_action( 'genesis_loop', 'genesis_do_loop' );
		
		//* Remove .site-inner
		add_filter( 'genesis_markup_site-inner', '__return_null' );
		add_filter( 'genesis_markup_content-sidebar-wrap_output', '__return_false' );
		add_filter( 'genesis_markup_content', '__return_null' );

		//* Add homepage widgets
		add_action( 'genesis_loop', 'eldorado_home_page_widgets' );
		
	}
}

function eldorado_home_page_widgets() {
	
	genesis_widget_area( 'home-top', array(
		'before' => '<div class="home-top"><div class="widget-area">',
		'after'  => '</div></div>',
	) );
	
	genesis_widget_area( 'home-middle', array(
		'before' => '<div class="home-middle widget-area"><div class="wrap">',
		'after'  => '</div></div>',
	) );
	
}

genesis();