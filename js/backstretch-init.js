jQuery(document).ready(function($) {
 
	// Create an array of images that you'd like to use
	var images = [
		'http://eldorado.dev/wp-content/themes/eldorado-theme/images/hometop-img-1.jpg',
		'http://eldorado.dev/wp-content/themes/eldorado-theme/images/hometop-img-2.jpg',
		'http://eldorado.dev/wp-content/themes/eldorado-theme/images/hometop-img-3.jpg'
	];
 
	$('body').backstretch(images, {duration:3000,fade:1500});

});