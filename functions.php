<?php
/**
 * Genesis Sample.
 *
 * This file adds functions to the Genesis Sample Theme.
 *
 * @package Genesis Sample
 * @author  StudioPress
 * @license GPL-2.0+
 * @link    http://www.studiopress.com/
 */

// Start the engine.
include_once( get_template_directory() . '/lib/init.php' );

// Setup Theme.
include_once( get_stylesheet_directory() . '/lib/theme-defaults.php' );

// Set Localization (do not remove).
add_action( 'after_setup_theme', 'genesis_sample_localization_setup' );
function genesis_sample_localization_setup(){
	load_child_theme_textdomain( 'genesis-sample', get_stylesheet_directory() . '/languages' );
}

// Add the helper functions.
include_once( get_stylesheet_directory() . '/lib/helper-functions.php' );

// Add Image upload and Color select to WordPress Theme Customizer.
require_once( get_stylesheet_directory() . '/lib/customize.php' );

// Include Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/output.php' );

// Add WooCommerce support.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-setup.php' );

// Add the required WooCommerce styles and Customizer CSS.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-output.php' );

// Add the Genesis Connect WooCommerce notice.
include_once( get_stylesheet_directory() . '/lib/woocommerce/woocommerce-notice.php' );

// Child theme (do not remove).
define( 'CHILD_THEME_NAME', 'Genesis Sample' );
define( 'CHILD_THEME_URL', 'http://www.studiopress.com/' );
define( 'CHILD_THEME_VERSION', '2.3.0' );

// Enqueue Scripts and Styles.
add_action( 'wp_enqueue_scripts', 'genesis_sample_enqueue_scripts_styles' );
function genesis_sample_enqueue_scripts_styles() {

	wp_enqueue_style( 'genesis-sample-fonts', '//fonts.googleapis.com/css?family=Lato:400,400i,700|Poppins:400,700|Source+Sans+Pro:400,600,700', array(), CHILD_THEME_VERSION );
	wp_enqueue_style( 'dashicons' );

	$suffix = ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) ? '' : '.min';
	wp_enqueue_script( 'genesis-sample-responsive-menu', get_stylesheet_directory_uri() . "/js/responsive-menus{$suffix}.js", array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_localize_script(
		'genesis-sample-responsive-menu',
		'genesis_responsive_menu',
		genesis_sample_responsive_menu_settings()
	);

}

// Define our responsive menu settings.
function genesis_sample_responsive_menu_settings() {

	$settings = array(
		'mainMenu'          => __( 'Menu', 'genesis-sample' ),
		'menuIconClass'     => 'dashicons-before dashicons-menu',
		'subMenu'           => __( 'Submenu', 'genesis-sample' ),
		'subMenuIconsClass' => 'dashicons-before dashicons-arrow-down-alt2',
		'menuClasses'       => array(
			'combine' => array(
				'.nav-primary',
				'.nav-header',
			),
			'others'  => array(),
		),
	);

	return $settings;

}

// Add HTML5 markup structure.
add_theme_support( 'html5', array( 'caption', 'comment-form', 'comment-list', 'gallery', 'search-form' ) );

// Add Accessibility support.
add_theme_support( 'genesis-accessibility', array( '404-page', 'drop-down-menu', 'headings', 'rems', 'search-form', 'skip-links' ) );

// Add viewport meta tag for mobile browsers.
add_theme_support( 'genesis-responsive-viewport' );

// Add support for after entry widget.
add_theme_support( 'genesis-after-entry-widget-area' );

// Remove content/sidebar/sidebar layout
genesis_unregister_layout( 'content-sidebar-sidebar' );
 
// Remove sidebar/sidebar/content layout
genesis_unregister_layout( 'sidebar-sidebar-content' );
 
// Remove sidebar/content/sidebar layout
genesis_unregister_layout( 'sidebar-content-sidebar' );

// Add support for 2-column footer widgets.
add_theme_support( 'genesis-footer-widgets', 2 );

// Add Image Sizes.
add_image_size( 'featured-image', 720, 400, TRUE ); //default
add_image_size( 'featured-image-thumbnail', 360, 360, TRUE ); //thumbnail for home page featured post

// Removing emoji code form head
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

// Enable shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

// Rename primary and secondary navigation menus.
add_theme_support( 'genesis-menus', array( 'primary' => __( 'After Header Menu', 'genesis-sample' ), 'secondary' => __( 'Footer Menu', 'genesis-sample' ) ) );

// Reposition the secondary navigation menu.
remove_action( 'genesis_after_header', 'genesis_do_subnav' );
add_action( 'genesis_footer', 'genesis_do_subnav', 5 );

// Reduce the secondary navigation menu to one level depth.
add_filter( 'wp_nav_menu_args', 'genesis_sample_secondary_menu_args' );
function genesis_sample_secondary_menu_args( $args ) {

	if ( 'secondary' != $args['theme_location'] ) {
		return $args;
	}

	$args['depth'] = 1;

	return $args;

}

// Remove the entry meta in the entry header
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

// Display Featured Image post title
add_action( 'genesis_entry_content', 'featured_post_image', 8 );
function featured_post_image() {
  if ( ! is_singular( 'post' ) )  return;
	the_post_thumbnail('post-image');
}

// Modify the WordPress - Genesis "read more" link for automatic excerpts & Featured Post - Translate it to different languages
add_filter( 'excerpt_more', 'child_read_more_link' );
add_filter( 'get_the_content_more_link', 'child_read_more_link' );
add_filter( 'the_content_more_link', 'child_read_more_link' );
function child_read_more_link() {
	if ( get_locale() == 'en_GB' ) {
		return '<span><a href="' . get_permalink() . '" class="more-link">Read&nbsp;more</a></span>';
	}
	elseif ( get_locale() == 'es_ES' ) {
		return '<span><a href="' . get_permalink() . '" class="more-link">Leer&nbsp;mas</a></span>';
	}
	elseif ( get_locale() == 'ru_RU' ) {
		return '<span><a href="' . get_permalink() . '" class="more-link">Читать&nbsp;далее</a></span>';
	}
	elseif ( get_locale() == 'fi' ) {
		return '<span><a href="' . get_permalink() . '" class="more-link">Lue&nbsp;lisää</a></span>';
	}
}

// Modify the WordPress read more link for hand-crafted excerpts - Translate it to different languages
add_filter('get_the_excerpt', 'wpm_manual_excerpt_read_more_link');
function wpm_manual_excerpt_read_more_link($excerpt) {
    $excerpt_more = '';
    if ( has_excerpt() && ! is_attachment() && get_post_type() == 'post' && get_locale() == 'en_GB' ) {
        $excerpt_more = '<span><a href="' . get_permalink() . '" class="more-link">Read&nbsp;more</a></span>';
    }
    elseif ( has_excerpt() && ! is_attachment() && get_post_type() == 'post' && get_locale() == 'es_ES' ) {
	    $excerpt_more = '<span><a href="' . get_permalink() . '" class="more-link">Leer&nbsp;mas</a></span>';
    }
    elseif ( has_excerpt() && ! is_attachment() && get_post_type() == 'post' && get_locale() == 'ru_RU' ) {
	    $excerpt_more = '<span><a href="' . get_permalink() . '" class="more-link">Читать&nbsp;далее</a></span>';
    }
    elseif ( has_excerpt() && ! is_attachment() && get_post_type() == 'post' && get_locale() == 'fi' ) {
	    $excerpt_more = '<span><a href="' . get_permalink() . '" class="more-link">Lue&nbsp;lisää</a></span>';
    }
    return $excerpt . $excerpt_more;
}

// Modify size of the Gravatar in the author box.
add_filter( 'genesis_author_box_gravatar_size', 'genesis_sample_author_box_gravatar' );
function genesis_sample_author_box_gravatar( $size ) {
	return 90;
}

// Modify size of the Gravatar in the entry comments.
add_filter( 'genesis_comment_list_args', 'genesis_sample_comments_gravatar' );
function genesis_sample_comments_gravatar( $args ) {

	$args['avatar_size'] = 60;

	return $args;

}

//* Customize the credits
add_filter( 'genesis_footer_creds_text', 'sp_footer_creds_text' );
function sp_footer_creds_text() {
	echo '<div class="creds"><p>';
	echo 'Copyright &copy; ';
	echo date('Y');
	echo ' &middot; Eldorado Tours &middot; All rights reserved';
	echo '</p></div>';
}

//* Register widget areas for home page
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home Top', 'eldorado' ),
	'description' => __( 'This is the home top section.', 'eldorado' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-middle',
	'name'        => __( 'Home Middle', 'eldorado' ),
	'description' => __( 'This is the home middle section.', 'eldorado' ),
) );